from django.contrib import admin
from .models import Project

# Register your models here.


# Define a custom admin class for the Project model
class ProjectAdmin(admin.ModelAdmin):
    # Define which fields should be displayed in the admin list view
    list_display = ["name", "description", "owner"]


admin.site.register(Project, ProjectAdmin)
