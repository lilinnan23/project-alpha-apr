from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import Project
from .forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/project_list.html", {"projects": projects}
    )


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(
                "list_projects"
            )  # Redirect to the list of projects
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
